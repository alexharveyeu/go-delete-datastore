package storage

import (
	"cloud.google.com/go/datastore"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/cloud"
)

// Datastore provides everything we need for sending requests to datastore.
type Client struct {
	Project string
	Context context.Context
	Client  *datastore.Client
}

// NewDatastore constructor for a datastore client. Needs a Google Cloud Project ID.
func NewClient(projectID string, namespace string) (*Client, error) {
	context, err := getContext(projectID, namespace)
	if err != nil {
		return nil, err
	}
	client, err := datastore.NewClient(context, projectID)
	if err != nil {
		return nil, err
	}
	return &Client{Project: projectID, Client: client, Context: context}, nil
}

func getContext(projectID string, namespace string) (ctx context.Context, err error) {
	ts, err := google.DefaultTokenSource(context.TODO(), datastore.ScopeDatastore)
	if err != nil {
		return nil, err
	}
	httpClient := oauth2.NewClient(context.TODO(), ts)
	ctx = cloud.NewContext(projectID, httpClient)
	if namespace != "" {
		ctx = datastore.WithNamespace(ctx, namespace)
	}
	return ctx, nil
}
