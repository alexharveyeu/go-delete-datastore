package main

import (
	"cloud.google.com/go/datastore"
	"gitlab.com/alexharveyeu/go-delete-datastore/storage"
	"log"
)

func main() {

	project := "compare-production"
	namespace := "testcategories"

	ds, err := storage.NewClient(project, namespace)
	if err != nil {
		log.Fatal(err)
	}

	var rows []struct {
		ChildKeys    []string
		Depth        int
		Key          string
		MasterKey    string
		Name         string
		ParentKey    string
		Path         string
		PathWithKeys string
		PathKeys     string
		Provider     string
	}

	var keys []*datastore.Key

	q := datastore.NewQuery("TestCategoryTwo").Project("Key")
	//q := datastore.NewQuery("Category").Filter("Provider =", "EBAY-GB")
	keys, err = ds.Client.GetAll(ds.Context, q, &rows)
	if err != nil {
		log.Fatalf("bad query, %v", err)
	}

	for _, k := range keys {
		err := ds.Client.Delete(ds.Context, k)
		if err != nil {
			log.Fatal(err)
		}
		log.Print(k)
	}

}
